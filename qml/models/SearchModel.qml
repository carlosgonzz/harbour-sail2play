import QtQuick 2.0

JSONListModel {
    property string search: ""
    source: search !== "" ? globals.apiurl + "/search/videos?search="+encodeURI(search)  + _getNSFWParam() : ""
    query: "$.data[*]"

    onSearchChanged: start = 0;

    function _getNSFWParam() {
        if (conf.showNSFW) {
            if (conf.showOnlyNSFW) return "&nsfw=true";
            return "";
        }
        return "&nsfw=false";
    }


    function filterJson(jo){
        // filter out nsfw result
        // (should be filtered by source url, but we'll keep it to be compatibile with older api)s
        if (!conf.showNSFW && jo.nsfw) return false;
        return true;
    }

}
