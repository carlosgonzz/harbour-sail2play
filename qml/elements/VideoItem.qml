import QtQuick 2.0
import Sailfish.Silica 1.0

import "../utils.js" as Utils

ListItem {
    id: delegate

    contentHeight: name.height + description.height + (Theme.paddingSmall * 2)
    anchors {
        left: parent.left
        right: parent.right
    }

    // TODO: add caching with Thumbnailer : https://sailfishos.org/develop/docs/nemo-qml-plugin-thumbnailer/
    Image {
        id: thumb
        source: globals.instanceurl + model.thumbnailPath
        height: Theme.iconSizeMedium
        width: height * 16 / 9
        anchors {
            leftMargin: Theme.paddingLarge
            topMargin: Theme.paddingSmall
            bottomMargin: Theme.paddingSmall
            left: parent.left
            top: parent.top
            bottom: parent.bottom
        }
    }

    Rectangle {
        id: durationbg
        color: "#000000"
        anchors {
            right: thumb.right
            bottom: thumb.bottom
        }
        width: duration.width + 10
        height: duration.height + 10
        Label {
            id:duration
            color: "#ffffff"
            text: Utils.s2str(model.duration)
            font.pixelSize: Theme.fontSizeTiny
            horizontalAlignment: Text.AlignRight
            verticalAlignment: Text.AlignTop
            anchors {
                top: parent.top
                left: parent.left
                topMargin: 5
                leftMargin: 5
            }
        }
    }

    Label {
        id: name
        text: model.name
        color: delegate.highlighted ? Theme.highlightColor : Theme.primaryColor
        truncationMode: TruncationMode.Fade
        // font.bold: true
        anchors {
            left: thumb.right
            top: parent.top
            right: parent.right
            topMargin: Theme.paddingSmall
            leftMargin: Theme.paddingMedium
            rightMargin: Theme.paddingMedium
        }
    }
    Label {
        id: description
        text: model.description
        color: delegate.highlighted ? Theme.secondaryHighlightColor : Theme.secondaryColor
        truncationMode: TruncationMode.Fade
        maximumLineCount: 1
        anchors {
            left: thumb.right
            top: name.bottom
            right: parent.right
            leftMargin: Theme.paddingMedium
            rightMargin: Theme.paddingMedium
        }
    }


}
