import Nemo.DBus 2.0


// https://git.merproject.org/mer-core/transfer-engine/blob/master/dbus/org.nemo.transferengine.xml
// https://sailfishos.org/develop/docs/nemo-qml-plugin-dbus/qml-nemo-dbus-dbusinterface.html
DBusInterface {
    bus: DBus.SystemBus
    service: "com.nokia.mce"
    iface: "com.nokia.mce.request"
    path: "/com/nokia/mce/request"


    property bool preventScreenBlanking : false

    onPreventScreenBlankingChanged: {
        if (preventScreenBlanking) {
            call("req_display_blanking_pause");
            console.log("Disabling display blanking");
        } else {
            call("req_display_cancel_blanking_pause");
            console.log("Enabling display blanking");
        }
    }
}
