import QtQuick 2.0
import com.blackgrain.qml.quickdownload 1.0

import QuickCross 1.0

Item {
    id: item

    TransferEngineCallback {
        id: tranferEngieCallback
        onCancel: {
            var index = -1;
            for(var k=0; k<downloads.count; k++) {
                var i = downloads.get(k);
                if (i.transferid === transferid) {
                    index = k;
                    break;
                }
            }
            if (index>-1) {
                downloaders.itemAt(index).stop()
            } else {
                // we don't have this transferId, sail2play could be crashed..
                // let's stop the transfer notification anyway
                transferEngine.finishTransferCancelled(transferid)
            }
        }
    }
    TransferEngine { id: transferEngine }

    ListModel { id: downloads }

    Repeater {
        id: downloaders
        model: downloads

        delegate: Item {
            Download {
                id: download

                url: model.url
                destination: model.destination

                running: false

                followRedirects: true
                onRedirected: console.log('Redirected',url,'->',redirectUrl)

                onStarted:console.log("Download started")
                onError: transferEngine.finishTransferInterrupted(model.transferid, errorString)
                onProgressChanged: transferEngine.updateTransferProgress(model.transferid, progress)
                onFinished: {
                    transferEngine.finishTransferSuccess(model.transferid)
                    downloads.remove(index)
                }
                Component.onCompleted: {
                    console.log("Start download", model.url, "->", model.destination, "#", model.transferid);
                    start();
                }
            }
            function stop() { download.stop(); }
        }
    }

    function get(title, url, size) {
        var videopath = StandardPaths.writableLocation(StandardPaths.MoviesLocation) + "/peertube";
        if (videopath === "") return;
        var filename = title.replace(/[^a-zA-Z0-9_\[\]\(\)-]/g," ").replace(/  +/g," ");
        var destination = videopath + "/" + filename + "." + url.split(".").pop();
        console.log("Downloader.get", title, url, size, "=>", destination);
        transferEngine.createDownload(title, url, size, function(transferid){
            transferEngine.startTransfer(transferid);
            downloads.append({'url':url, 'destination': destination, 'transferid': transferid})
        });
    }
}
