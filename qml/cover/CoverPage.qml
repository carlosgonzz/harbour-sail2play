import QtQuick 2.0
import Sailfish.Silica 1.0

CoverBackground {

    // video preview
    Image {
        id: preview
        source: (globals.instanceurl && globals.currentVideo) ? globals.instanceurl + globals.currentVideo.data.previewPath : ""
        anchors.fill: parent
        fillMode: Image.PreserveAspectCrop
    }

    // video list
    Column {
        id: iconInstance

        anchors.verticalCenter: parent.verticalCenter
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.margins: Theme.paddingMedium
        spacing: Theme.paddingMedium

        Image {
            source: "/usr/share/icons/hicolor/128x128/apps/harbour-sail2play.png"
            anchors.horizontalCenter: parent.horizontalCenter
        }

        Label {
            id: labelInstance
            text: globals.instance
            width: parent.width
            truncationMode: TruncationMode.Fade
        }
    }



    state: appState


    states: [
        State {
            name: ""
            PropertyChanges { target: iconInstance; visible: true; opacity: 1.0 }
            PropertyChanges { target: labelInstance; visible: false }
            PropertyChanges { target: preview; visible: false }
        },
        State {
            name: "videolist"
            PropertyChanges { target: iconInstance; visible: true; opacity: 1.0  }
            PropertyChanges { target: labelInstance; visible: true }
            PropertyChanges { target: preview; visible: false }
        },
        State {
            name: "videodetails"
            PropertyChanges { target: iconInstance; visible: false; opacity: 0.5  }
            PropertyChanges { target: labelInstance; visible: false }
            PropertyChanges { target: preview; visible: true }
        },
        State {
            name: "videoplayer"
            PropertyChanges { target: iconInstance; visible: false; opacity: 0.5  }
            PropertyChanges { target: labelInstance; visible: false }
            PropertyChanges { target: preview; visible: true }
        }
    ]



/*
    CoverActionList {
        id: coverAction

        CoverAction {
            iconSource: "image://theme/icon-cover-search"
            onTriggered: {
                showVideoSearchView();
            }
        }

    }
    */
}
