import QtQuick 2.0
import Sailfish.Silica 1.0

import "../elements"
import "../models"

Page {
    id: page

    // The effective value will be restricted by ApplicationWindow.allowedOrientations
    allowedOrientations: Orientation.All

    function onThisPage() {
        globals.instance = ""
        globals.instanceurl = ""
        console.log(applicationWindow.cover);
    }


    Component.onCompleted: {
        console.log(args.length, "args", args);
        if (args.length === 2) {
            pageStack.completeAnimation();
            stupidOpenUrlOnStartTimeout.url = args[1];
            stupidOpenUrlOnStartTimeout.running = true;
        } else {
            loadingAtStart.visible = false;
        }
    }

    JSONListModel {
        id: bookmarksModel
        json: conf.bookmarks
     }

    SilicaListView {
        // PullDownMenu and PushUpMenu must be declared in SilicaFlickable, SilicaListView or SilicaGridView
        PullDownMenu {
            MenuItem {
                text: qsTr("Settings")
                onClicked: pageStack.push(Qt.resolvedUrl("Preferences.qml"));
            }

            MenuItem {
                text: qsTr("Add instance")
                onClicked: pageStack.push(Qt.resolvedUrl("AddInstance.qml"));
            }

            // TODO: move this to settings page
            /*MenuItem {
                text: "Reset Bookmarks"
                onClicked: conf.resetBookmarks()
            }*/

            MenuItem {
                text: qsTr("Browse instances")
                onClicked: pageStack.push(Qt.resolvedUrl("InstancesList.qml"));
            }
            MenuItem {
                text: qsTr("Open from clipboard")
                enabled: Clipboard.hasText && Clipboard.text.indexOf("/videos/watch/")>0
                onClicked: {
                    var url = Clipboard.text;
                    openUrl(url);
                }
            }
        }


        id: listView
        model: bookmarksModel.model
        anchors.fill: parent
        header: PageHeader {
            title: qsTr("Bookmarks")
        }
        delegate: InstanceItem{
            id: delegate
            simple: true
            onClicked: openBookmark(index)
            menu: ContextMenu {
                MenuItem {
                    text: qsTr("Remove from bookmarks")
                    RemorseItem { id: remorse }
                    function showRemorseItem() {
                        var idx = index
                        remorse.execute(delegate, qsTr("Removing bookmark"), function() { conf.removeBookmark(idx) } )
                    }
                    onClicked: showRemorseItem()
                }
            }
        }
        VerticalScrollDecorator {}
    }

    Rectangle {
        id: loadingAtStart
        color: Theme.highlightBackgroundColor
        opacity: Theme.highlightBackgroundOpacity
        anchors.fill: parent;
        BusyIndicator {
            size: BusyIndicatorSize.Large
            anchors.centerIn: parent
            running: true
        }
    }

    function openBookmark(idx) {
        console.log("openBookmark", idx)
        var obj = globals.bookmarks[idx];
        globals.instance = obj.name
        globals.instanceurl = "https://" + obj.host
        console.log("pageStack VideoList");
        pageStack.push(Qt.resolvedUrl("VideoList.qml"));
    }

    function openUrl(url) {
        console.log("openUrl", url);
        if (url.indexOf("/videos/watch/") < 0) return;
        var toks = url.split("/videos/watch/");
        console.log("openUrl toks", toks);
        globals.instanceurl = toks[0];
        console.log("pageStack VideoDetails uuid:", toks[1]);
        pageStack.push(Qt.resolvedUrl("VideoDetails.qml"), {uuid:toks[1]});
    }

    Timer {
        property string url: ""

        id: stupidOpenUrlOnStartTimeout
        interval: 1
        running: false;
        repeat: false;
        onTriggered: {
            loadingAtStart.visible = false;
            openUrl(url);
        }

    }

}
