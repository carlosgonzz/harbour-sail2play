import QtQuick 2.0
import Sailfish.Silica 1.0

Dialog {
    Column {
        width: parent.width

        DialogHeader { }

        TextField {
            id: instanceHost
            width: parent.width
            placeholderText: qsTr("Instance host name")
            label: qsTr("Instance host name")
        }

        TextField {
            id: instanceName
            width: parent.width
            placeholderText: qsTr("Instance name")
            label: qsTr("Instance name")
        }
    }

    onDone: {
        if (result == DialogResult.Accepted) {
            conf.addBookmark({ name: instanceName.text, host: instanceHost.text });
        }
    }
}
