import QtQuick 2.0
import Sailfish.Silica 1.0
import "../models"
import "../elements"
Page {
    id: page

    property string listorder: "trending"
    property string listfilter: ""

    property string title: qsTr("Trending")
    property string description: "";

    // The effective value will be restricted by ApplicationWindow.allowedOrientations
    allowedOrientations: Orientation.All

    function onThisPage() {
        applicationWindow.cover.state = "videolist"
    }

    VideoListModel{
        id: videoListModel
    }

    S2PListView {
        id: listView

        PullDownMenu {
            MenuItem {
                text: qsTr("Trending")
                onClicked: {
                    listorder = "trending"
                    videoListModel.orderTrending();
                    title = qsTr("Trending")
                }
               font.bold: listorder === "trending"
            }
            MenuItem {
                text: qsTr("Recent")
                onClicked: {
                    listorder = "recent"
                    videoListModel.orderRecent();
                    title = qsTr("Recent")
                }
               font.bold: listorder === "recent"
            }
            MenuItem {
                text: listfilter === "local" ? qsTr("Show all") : qsTr("Show local")
                onClicked: {
                    if (listfilter === "local") {
                        listfilter =  "";
                        description = "";
                        videoListModel.filterAll();
                    } else {
                        listfilter = "local";
                        description = qsTr("local");
                        videoListModel.filterLocal();
                    }
                }
            }

            MenuItem {
                text: qsTr("Search")
                onClicked:pageStack.push(Qt.resolvedUrl("SearchPage.qml"));
            }
        }

        model: videoListModel.model
        anchors.fill: parent

        onAtYEndChanged: {
            if (atYEnd) videoListModel.loadMore();
        }

        header: PageHeader {
            id: pageHeader
            title: page.title
            description: page.description
        }

        delegate: VideoItem {
            onClicked: pageStack.push(Qt.resolvedUrl("VideoDetails.qml"), {uuid:model.uuid})
        }

        ViewPlaceholder {
            enabled: listView.count == 0 && videoListModel.status > 0
            text: qsTr("No items")
            hintText: qsTr("Try to select another instance")
        }

        VerticalScrollDecorator {}
    }

    LoadingSpinner {
        model: videoListModel
    }

}
