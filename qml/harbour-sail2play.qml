import QtQuick 2.0
import Sailfish.Silica 1.0
import Nemo.Configuration 1.0

import "system"
import "pages"

ApplicationWindow
{
    id: applicationWindow
    property int kStandardAnimationDuration: 200
    property string appState: ""

    Item {
        id: globals
        property string instance : ""
        property string instanceurl: ""
        property string apiurl: instanceurl+"/api/v1"
        property variant currentVideo: null

        property int paginationCount: 20

        property variant bookmarks: []
    }

    ConfigurationGroup {
        id: conf
        path: "/apps/harbour-sai2play"

        property int preferredQuality: 480
        property bool autoPlay: true

        property int preferredDownloadQuality: 720

        property string bookmarks: '[{"name":"PeerTube.video","host":"peertube.video"},{"name":"peertube.social","host":"peertube.social"},{"name":"FramaTube","host":"framatube.org"},{"name":"Exode... a musicians instance","host":"exode.me"}]'
        property int autoopen: -1

        property bool showNSFW: false
        property bool showOnlyNSFW: false

        function resetBookmarks() {
            bookmarks= '[{"name":"PeerTube.video","host":"peertube.video"},{"name":"peertube.social","host":"peertube.social"},{"name":"FramaTube","host":"framatube.org"},{"name":"Exode... a musicians instance","host":"exode.me"}]';
        }

        function addBookmark(obj) {
            globals.bookmarks.push(obj)
            bookmarks = JSON.stringify(globals.bookmarks);
        }

        function removeBookmark(idx) {
            globals.bookmarks.splice(idx, 1);
            if (idx === autoopen) autoopen = -1
            bookmarks = JSON.stringify(globals.bookmarks);
        }


        onValueChanged: {
            console.log(key);
            globals.bookmarks = JSON.parse(bookmarks); // still not nice..
        }
        Component.onCompleted: {
            console.log(bookmarks);
            globals.bookmarks = JSON.parse(bookmarks);
        }

    }

    Downloader { id: downloader }

    MCE { id:mce }

    initialPage: Component { Bookmarks { } }
    cover: Qt.resolvedUrl("cover/CoverPage.qml")
    allowedOrientations: defaultAllowedOrientations


    function showVideoSearchView() {

        pageStack.find(function(page){
            console.log(page);
        });
    }




    Component.onCompleted: {
        pageStack.onCurrentPageChanged.connect(function(){
            var cbk =  pageStack.currentPage.onThisPage;
            if (cbk !== undefined) cbk();
        })
    }


}
