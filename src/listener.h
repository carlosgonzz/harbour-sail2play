#ifndef LISTENER_H
#define LISTENER_H

#include <QObject>
#include <QNetworkSession>
#include <QTcpServer>

#include "proxy.h"

class Listener : public QObject
{
    Q_OBJECT
public:
    explicit Listener(QObject *parent = nullptr);

signals:

public slots:
    void close();

private slots:
    void sessionError(QNetworkSession::SessionError error);
    void sessionOpened();
    void newConnection();
    void proxyDone();

private:
    QNetworkSession *networkSession = nullptr;
    QTcpServer *tcpServer = nullptr;

    QList<QSharedPointer<Proxy>> proxies;
};

#endif // LISTENER_H
