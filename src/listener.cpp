#include <QtNetwork>
#include <QtCore>
#include <QtDebug>

#include "listener.h"
#include "proxy.h"

Listener::Listener(QObject *parent) : QObject(parent)
{
    qDebug() << "Listener starting";
/*    QNetworkConfigurationManager manager;
    if (manager.capabilities() & QNetworkConfigurationManager::NetworkSessionRequired) {
        qDebug() << ".. with saved network configuration";
        // Get saved network configuration
        QSettings settings(QSettings::UserScope, QLatin1String("QtProject"));
        settings.beginGroup(QLatin1String("QtNetwork"));
        const QString id = settings.value(QLatin1String("DefaultNetworkConfiguration")).toString();
        settings.endGroup();

        // If the saved network configuration is not currently discovered use the system default
        QNetworkConfiguration config = manager.configurationFromIdentifier(id);
        if ((config.state() & QNetworkConfiguration::Discovered) !=
            QNetworkConfiguration::Discovered) {
            config = manager.defaultConfiguration();
        }

        networkSession = new QNetworkSession(config, this);
        connect(networkSession, &QNetworkSession::opened, this, &Listener::sessionOpened);
        // connect(networkSession, &QNetworkSession::error, this, &Listener::sessionError);
        connect(networkSession, SIGNAL(error(QNetworkSession::SessionError)), SLOT(sessionError(QNetworkSession::SessionError)));
        networkSession->open();
    } else {
        sessionOpened();
    }
*/
    // I have issues with QNetworkSession.. let's just start the server
    sessionOpened();
}

void Listener::close()
{
    qDebug()<< "Listener::close";
    if (tcpServer != nullptr) {
        tcpServer->close();
        tcpServer = nullptr;
    }

    proxies.clear();
}

void Listener::sessionError(QNetworkSession::SessionError /*error*/)
{
   if (networkSession) qDebug() << "Error: " << networkSession->errorString();
}

void Listener::sessionOpened()
{
    qDebug() << "starting session";
    // Save the used configuration
    if (networkSession) {
        QNetworkConfiguration config = networkSession->configuration();
        QString id;
        if (config.type() == QNetworkConfiguration::UserChoice)
            id = networkSession->sessionProperty(QLatin1String("UserChoiceConfiguration")).toString();
        else
            id = config.identifier();

        QSettings settings(QSettings::UserScope, QLatin1String("QtProject"));
        settings.beginGroup(QLatin1String("QtNetwork"));
        settings.setValue(QLatin1String("DefaultNetworkConfiguration"), id);
        settings.endGroup();
    }

    tcpServer = new QTcpServer(this);
    if (!tcpServer->listen(QHostAddress::LocalHost, 56740)) {
        // TODO report error tcpServer->errorString();
        qDebug() << "Error: " << tcpServer->errorString();
        return;
    }

    connect(tcpServer, &QTcpServer::newConnection, this, &Listener::newConnection);
    qDebug() << "tcpServer on " << tcpServer->serverAddress() << ":" << tcpServer->serverPort();
}

void Listener::newConnection()
{
    QTcpSocket *con = tcpServer->nextPendingConnection();
    qDebug("spawn new proxy");
    Proxy *p = new Proxy(con, proxies.length()); //OK I don't know C++ really, nor Qt...
    proxies.append(QSharedPointer<Proxy>(p));
    connect(p, &Proxy::done, this, &Listener::proxyDone);

}

void Listener::proxyDone()
{
    Proxy *p = qobject_cast<Proxy*>(sender());
    // ... and I cant't manage to remove the proxy object I got from sender() from proxyes.
    // so I'm saving the object index in list in 'id'
    int i = p->getId();
    if (i!=-1) proxies.removeAt(i);
}


