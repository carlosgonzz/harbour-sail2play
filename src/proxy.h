#ifndef REQUESTER_H
#define REQUESTER_H

#include <QObject>
#include <QNetworkAccessManager>
#include <QTcpSocket>


class Proxy : public QObject
{
    Q_OBJECT
public:
    Proxy(QTcpSocket *request, int id);
    int getId() { return id; }

private slots:
    void requestDataReady();
    void httpMetadata();
    void httpReadyRead();
    void httpFinished();
#ifndef QT_NO_SSL
    void sslErrors(QNetworkReply *, const QList<QSslError> &errors);
#endif

signals:
    void done();

private:
    int id;
    QNetworkAccessManager qnam;
    QNetworkReply *reply = nullptr;
    QTcpSocket *request;
};

#endif // REQUESTER_H
