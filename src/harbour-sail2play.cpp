#ifdef QT_QML_DEBUG
#include <QtQuick>
#endif

#include <QGuiApplication>
#include <QQmlEngine>
#include <QQuickView>

#include "listener.h"
#include <sailfishapp.h>
#include <QStandardPaths>
#include <QDir>
#include "qcstandardpaths.h"


static QObject *standardPathsProvider(QQmlEngine *engine, QJSEngine *scriptEngine) {
    Q_UNUSED(engine);
    Q_UNUSED(scriptEngine);

    QCStandardPaths* object = new QCStandardPaths();

    return object;
}

int main(int argc, char *argv[])
{


    // SailfishApp::main() will display "qml/harbour-sail2play.qml", if you need more
    // control over initialization, you can use:
    //
    //   - SailfishApp::application(int, char *[]) to get the QGuiApplication *
    //   - SailfishApp::createView() to get a new QQuickView * instance
    //   - SailfishApp::pathTo(QString) to get a QUrl to a resource file
    //   - SailfishApp::pathToMainQml() to get a QUrl to the main QML file
    //
    // To display the view, call "show()" (will show fullscreen on device).



    // return SailfishApp::main(argc, argv);

    QGuiApplication *app = SailfishApp::application(argc, argv);
    QQuickView *view = SailfishApp::createView();


    // create default download path
    QDir moviedir(QStandardPaths::writableLocation(QStandardPaths::MoviesLocation));
    if (!moviedir.exists("peertube")) {
        moviedir.mkdir("peertube");
    }


    // QCStandardPaths from https://github.com/benlau/quickcross/
    // I don't understand how this isn't standard...
    qmlRegisterSingletonType<QCStandardPaths>("QuickCross", 1, 0, "StandardPaths", standardPathsProvider);

    // set args property
    QStringList qargv;
    for(int k=0; k < argc; k++) {
        QString a(argv[k]);
        qargv << a;
    }
    view->rootContext()->setContextProperty("args", qargv);



    view->setSource(SailfishApp::pathToMainQml());
    view->show();

    // stupid http proxy to bypass old gnutls used by gstreamer
    // listen on localhost:56740
    Listener l;


    return app->exec();

}

