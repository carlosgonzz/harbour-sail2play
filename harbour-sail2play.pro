# NOTICE:
#
# Application name defined in TARGET has a corresponding QML filename.
# If name defined in TARGET is changed, the following needs to be done
# to match new name:
#   - corresponding QML filename must be changed
#   - desktop icon filename must be changed
#   - desktop filename must be changed
#   - icon definition filename in desktop file must be changed
#   - translation filenames have to be changed

# The name of your application
TARGET = harbour-sail2play

CONFIG += sailfishapp

SOURCES += src/harbour-sail2play.cpp \
    src/listener.cpp \
    src/proxy.cpp \
    src/qcstandardpaths.cpp

DISTFILES += qml/harbour-sail2play.qml \
    qml/cover/CoverPage.qml \
    rpm/harbour-sail2play.changes.in \
    rpm/harbour-sail2play.changes.run.in \
    rpm/harbour-sail2play.spec \
    rpm/harbour-sail2play.yaml \
    harbour-sail2play.desktop \
    harbour-sail2play-url.desktop \
    translations/*.ts \
    qml/pages/Bookmarks.qml \
    qml/pages/VideoList.qml \
    qml/elements/VideoItem.qml \
    qml/models/SearchModel.qml \
    qml/pages/SearchPage.qml \
    qml/pages/VideoPlayer.qml \
    qml/models/JSONObjectData.qml \
    qml/models/VideoData.qml \
    qml/utils.js \
    qml/pages/VideoDetails.qml \
    qml/models/InstancesModel.qml \
    qml/pages/InstancesList.qml \
    qml/elements/LoadingSpinner.qml \
    qml/elements/S2PListView.qml \
    qml/elements/InstanceItem.qml \
    icons/scalable/harbour-sail2play.svg \
    qml/pages/AddInstance.qml \
    qml/pages/Preferences.qml \
    qml/models/VideoListModel.qml \
    qml/system/TransferEngine.qml \
    qml/system/Downloader.qml \
    qml/system/TransferEngineCallback.qml \
    qpm.json \
    qml/system/MCE.qml

SAILFISHAPP_ICONS = 86x86 108x108 128x128 172x172


urlfile.files = harbour-sail2play-url.desktop
urlfile.path = /home/nemo/.local/share/applications

INSTALLS += urlfile



# to disable building translations every time, comment out the
# following CONFIG line
CONFIG += sailfishapp_i18n

# German translation is enabled as an example. If you aren't
# planning to localize your app, remember to comment out the
# following TRANSLATIONS line. And also do not forget to
# modify the localized app name in the the .desktop file.
TRANSLATIONS += translations/harbour-sail2play-it.ts

HEADERS += \
    src/listener.h \
    src/proxy.h \
    src/qcstandardpaths.h

include(vendor/vendor.pri)
