<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1">
<context>
    <name>AddInstance</name>
    <message>
        <source>Instance host name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Instance name</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Bookmarks</name>
    <message>
        <source>Bookmarks</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Browse instances</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Open from clipboard</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Remove from bookmarks</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Add instance</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Removing bookmark</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Settings</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>InstancesList</name>
    <message>
        <source>No instances found</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>That&apos;s odd.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>PeerTube Instances</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Add to bookmarks</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Remove from bookmarks</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Removing bookmark</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Search</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Preferences</name>
    <message>
        <source>Video player</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Preferred video quality</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Autoplay video</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Bookmarks</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Reset bookmarks</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Preferred video download quality</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Contents</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Show NSWF in results</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Show Only NSWF</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SearchPage</name>
    <message>
        <source>Search</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>No items</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>VideoDetails</name>
    <message>
        <source>Play</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Open in browser</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Copy URL to clipboard</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Download video</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>VideoList</name>
    <message>
        <source>Search</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Trending</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>No items</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Try to select another instance</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Recent</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>local</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Show all</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Show local</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>VideoPlayer</name>
    <message>
        <source>Error:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Video resolution</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
